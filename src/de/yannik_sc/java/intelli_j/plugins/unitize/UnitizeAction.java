package de.yannik_sc.java.intelli_j.plugins.unitize;

import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.editor.Editor;
import com.intellij.psi.PsiFile;
import de.yannik_sc.java.intelli_j.plugins.unitize.components.UnitizeService;
import org.jetbrains.annotations.Nullable;

/**
 * Plugin unitize Action
 *
 * @author Yannik Schwieger.
 */
public class UnitizeAction extends AnAction
{
    @Override
    public void actionPerformed(AnActionEvent event)
    {
        Editor editor = this.getEditor(event);

        if (editor == null) {
            return;
        }

        UnitizeService unitizer = new UnitizeService();

        int linePos = editor.getCaretModel().getLogicalPosition().line;
        int currentPos = editor.getDocument().getLineStartOffset(linePos);
        int endPos = 0;

        try {
            endPos = editor.getDocument().getLineStartOffset(linePos + 1);
        } catch(IndexOutOfBoundsException exception) {
            return;
        }

        String line = editor.getDocument().getCharsSequence().subSequence(currentPos, endPos).toString();

        if (!unitizer.isUnitizable(line.replace(" ", ""))) {
            return;
        }

        unitizer.writeLine(editor, currentPos, endPos, unitizer.unitizeLine(line) + "\n");
    }

    /**
     * Returns the active editor if a less file is selected (otherwise returns null)
     *
     * @param event
     * @return
     */
    @Nullable
    protected Editor getEditor(AnActionEvent event)
    {
        PsiFile file;

        try {
            file = event.getRequiredData(CommonDataKeys.PSI_FILE);
        } catch (Exception e) {
            return null;
        }

        if (!file.getVirtualFile().getName().endsWith(".less")) {
            return null;
        }

        Editor editor;

        try {
            editor = event.getRequiredData(CommonDataKeys.EDITOR);
        } catch (Exception e) {
            return null;
        }
        return editor;
    }
}
