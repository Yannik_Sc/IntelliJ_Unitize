package de.yannik_sc.java.intelli_j.plugins.unitize.components;

import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Editor;
import de.yannik_sc.java.intelli_j.plugins.unitize.handler.ICSSPropertyHandler;

import java.util.*;

/**
 * This file takes care about the css replacing
 *
 * @author Yannik_Sc.
 */
public class UnitizeService
{
    private static final String REGEX = "([a-z\\-]+):( *?)([0-9\\-]+)px;(\\s\\S*?)";

    protected Map<String, ICSSPropertyHandler> handler = new HashMap<>();

    public UnitizeService()
    {
        this.handler.put("margin", this::handleMargin);
        this.handler.put("padding", this::handlePadding);
    }

    /**
     * Checks if a line is unitizable
     *
     * @param line
     * @return
     */
    public boolean isUnitizable(String line)
    {
        if (line.contains("@nounitize")) {
            return false;
        }

        if (line.matches(UnitizeService.REGEX)) {
            return true;
        }

        return this.handler.containsKey(line.split(":")[0]);

    }

    /**
     * Unitizes a single line
     *
     * @param line
     * @return
     */
    public String unitizeLine(String line)
    {
        StringBuilder spaces = new StringBuilder();

        while (line.indexOf(" ") == 0) {
            line = line.substring(1);
            spaces.append(" ");
        }

        if (line.matches(UnitizeService.REGEX) && !this.handler.containsKey(line.split(":")[0])) {
            line = this.unitize(line);
        }

        if (this.handler.containsKey(line.split(":")[0])) {
            line = this.handler.get(line.split(":")[0]).handle(line);
        }

        line = spaces + line;

        return line;
    }

    /**
     * Unitizes general css properties (e.g. width: 12px)
     *
     * @param line
     * @return
     */
    protected String unitize(String line)
    {
        line = line.replaceAll(UnitizeService.REGEX, ".unitize($1, $3);");

        return line;
    }

    /**
     * Handles the unitize of the margin property
     *
     * @param line
     * @return
     */
    protected String handleMargin(String line)
    {
        String value = this.unitizeFourValues(line);

        return String.format(".unitize-margin(%s);", value.replace("px", ""));
    }

    /**
     * Handles the unitize of the margin property
     *
     * @param line
     * @return
     */
    protected String handlePadding(String line)
    {
        String value = this.unitizeFourValues(line);

        return String.format(".unitize-padding(%s);", value.replace("px", ""));
    }

    /**
     * Minimizes four css values (e.g. 12px 13px 12px 13px to 12px 13px)
     *
     * @param line
     * @return
     */
    protected String unitizeFourValues(String line)
    {
        String[] values = line.split(":")[1].trim().replace(";", "").split(" ");
        List<String> normalizedValues = new ArrayList<>();

        if (values.length == 1) {
            normalizedValues = this.getValues(values, 0, 0);
        }

        if (values.length == 4) {
            normalizedValues = Arrays.asList(values);
        }

        if (values.length == 3 ||
            (values.length == 4 && values[1].equals(values[3]))
            ) {
            normalizedValues = this.getValues(values, 0, 1, 2);
        }

        if (values.length == 2 ||
            (values.length == 3 && values[0].equals(values[2])) ||
            (values.length == 4 && values[0].equals(values[2]) && values[1].equals(values[3]))
            ) {
            normalizedValues = this.getValues(values, 0, 1);
        }

        String value = normalizedValues.toString();
        value = value.substring(1, value.length() - 1);
        return value;
    }

    /**
     * Returns a list filled with the given values, replaces px with nothing
     *
     * @param values
     * @param numbers
     * @return
     */
    protected List<String> getValues(String[] values, int... numbers)
    {
        List<String> normalizedValues = new ArrayList<>();

        for (int number : numbers) {
            normalizedValues.add(values[number].replace("px", "").trim());
        }

        return normalizedValues;
    }

    /**
     * Writes a line at the given position to the given editor
     *
     * @param editor
     * @param currentPos
     * @param endPos
     * @param line
     */
    public void writeLine(Editor editor, int currentPos, int endPos, String line)
    {
        WriteCommandAction.runWriteCommandAction(editor.getProject(), () -> {
            editor.getDocument().replaceString(currentPos, endPos, line);
            this.nextLine(editor);
        });
    }

    /**
     * Moves the cursor to the next line
     *
     * @param editor
     */
    protected void nextLine(Editor editor)
    {
        int linePos = editor.getCaretModel().getLogicalPosition().line;
        int lineEnd = editor.getDocument().getLineEndOffset(linePos + 1);
        editor.getCaretModel().moveToOffset(lineEnd);
    }
}
