package de.yannik_sc.java.intelli_j.plugins.unitize.handler;

/**
 * @author Yannik Schwieger.
 */
public interface ICSSPropertyHandler
{
    String handle(String line);
}
