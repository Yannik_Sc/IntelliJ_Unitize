# IntelliJ Unitize Plugin

## About
This plugin wraps CSS properties in the unitize mixin from Shopware in less files by pressing Ctrl + U e.g.

```LESS
.test {
    margin: 10px 0 10px;
    /* Becomes */
    .unitize-margin(10px, 0);
}
```

With the `@nounitize` CSS annotation (© Yannik_Sc) you can disable the unitizer for a single line.

By pressing Ctrl + Shift + Alt + U, you can unitize a whole file.

## [License](LICENSE)
This Project uses the GPLv3 License