package de.yannik_sc.java.intelli_j.plugins.unitize;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.editor.Editor;
import com.intellij.psi.PsiFile;
import de.yannik_sc.java.intelli_j.plugins.unitize.components.UnitizeService;
import org.jetbrains.annotations.Nullable;

/**
 * This file is part of the Y_Frame
 *
 * @author Yannik_Sc.
 */
public class UnitizeFileAction extends AnAction
{
    protected Editor editor;

    @Override
    public void actionPerformed(AnActionEvent event)
    {
        Editor editor = this.getEditor(event);

        if (editor == null) {
            return;
        }

        this.editor = editor;

        UnitizeService unitizer = new UnitizeService();

        int firstLine = 1;
        int lastLine = editor.getDocument().getLineCount() - 1;

        for (int linePos = firstLine; linePos < lastLine; linePos++) {
            int[] position = this.lineToPositions(linePos);
            int lineStart = position[0];
            int lineEnd = position[1];

            String line = editor.getDocument().getCharsSequence().subSequence(lineStart, lineEnd).toString();

            if (!unitizer.isUnitizable(line.replace(" ", ""))) {
                continue;
            }

            unitizer.writeLine(editor, lineStart, lineEnd, unitizer.unitizeLine(line) + "\n");
        }

    }

    /**
     * Returns the active editor if a less file is selected (otherwise returns null)
     *
     * @param event
     * @return
     */
    @Nullable
    protected Editor getEditor(AnActionEvent event)
    {
        try {
            PsiFile file = event.getRequiredData(CommonDataKeys.PSI_FILE);

            if (!file.getVirtualFile().getName().endsWith(".less")) {
                return null;
            }
        } catch (Exception e) {
            return null;
        }

        Editor editor;

        try {
            editor = event.getRequiredData(CommonDataKeys.EDITOR);
        } catch (Exception e) {
            return null;
        }
        return editor;
    }

    /**
     * Gets the start and end position from a line
     *
     * @param line
     * @return
     */
    protected int[] lineToPositions(int line)
    {
        int lineStart = this.editor.getDocument().getLineStartOffset(line);
        int lineEnd = editor.getDocument().getLineStartOffset(line + 1);

        return new int[]{lineStart, lineEnd};
    }
}
